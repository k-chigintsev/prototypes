/*
Copyright (c) 2016, Konstantin Chigintsev <kchigintsev@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "immutable_map.hpp"


template<typename K, typename V>
void print_map(const wheel::immutable_map<K, V>& m, const std::string& comment) {
  typedef typename wheel::immutable_map<K, V>::const_iterator iterator;
  std::cout << comment << ": ";
  for (iterator it = m.begin(), end = m.end(); it != end; ++it) {
    std::cout << "(" << it->first << ", " << (*it).second << ") ";
  }
  std::cout << std::endl;
  std::cout.flush();
}

void test_consistency(std::size_t size, std::vector<std::size_t>& data) {

  typedef wheel::immutable_map<std::size_t, std::size_t> map_type;

  map_type m;
  map_type halfway;
  assert(m.size() == 0 && "garbage size #1");
  for (std::size_t i = 0; i < size; ++i) {
    m = m.insert(data[i], i);
    assert(m.size() == (i+1) && "size mismatch");
  }
  halfway = m;
  assert(halfway.size() == size && "size mismatch after assignment");
  assert(m.size() == halfway.size() && "size mismatch after assignment");
  for (std::size_t i = size; i < 2 * size; ++i) {
    m = m.insert(data[i], i);
    assert(m.size() == (i+1) && "size mismatch #2");
  }
  map_type full = m;
  assert(halfway.size() == size && "size mismatch after assignment #2");
  assert(full.size() == (2*size) && "size mismatch after assignment #2");
  assert(m.size() == full.size() && "size mismatch after assignment #2");

  typedef map_type::const_iterator iterator;
  for (std::size_t i = 0; i < size; ++i) {
    iterator it = m.find(data[i]);
    assert(it != m.end() && "element was not inserted correctly");
    m = m.erase(data[i]);
    it = m.find(data[i]);
    assert(it == m.end() && "element was not removed correctly");

    assert(m.size() == (2*size - (i+1)) && "size mismatch #3");
    full = full.insert(data[i], -i-1);
  }
  map_type second_half = m;
  for (std::size_t i = size; i < 2*size; ++i) {
    m = m.erase(data[i]);
    assert(m.size() == (2*size - (i+1)) && "size mismatch #4");
  }
  assert(m.begin() == m.end() && m.size() == 0 && "not empty again");

  for (std::size_t i = 0; i < size; ++i) {
    iterator it1 = halfway.find(data[i]);
    assert(it1 != halfway.end() && "lost key");
    assert(it1->first == data[i] && it1->second == i && "key/value mismatch");

    iterator it2 = full.find(data[i]);
    assert(it2 != full.end() && "lost key #2");
    assert(it2->first == data[i] && it2->second == (-i-1) && "key/value mismatch #2");

    iterator it3 = second_half.find(data[i]);
    assert(it3 == second_half.end() && "not deleted key");
  }

  for (std::size_t i = size; i < 2*size; ++i) {
    iterator it1 = halfway.find(data[i]);
    assert(it1 == halfway.end() && "not deleted key #2");

    iterator it2 = full.find(data[i]);
    assert(it2 != full.end() && "lost key #2");
    assert(it2->first == data[i] && it2->second == i && "key/value mismatch #3");

    iterator it3 = second_half.find(data[i]);
    assert(it3->first == data[i] && it3->second == i && "key/value mismatch #4");
  }

  map_type dummy;
  halfway = dummy;
  second_half = dummy;

  assert(full.order_statistic(-1) == full.end() && "order statitics garbage index");
  assert(full.order_statistic(full.size()) == full.end() && "order statitics garbage index #2");
  std::size_t idx = 0;
  std::size_t max_height = 0;
  for (iterator it = full.begin(), end = full.end(); it != end; ++it, ++idx) {
    assert(it->first == idx && "wrong key order");

    iterator ost_it = full.order_statistic(idx);
    assert(ost_it != end && "order statistic must be found");
    assert(ost_it->first == idx && "wrong order statistic");
    if (ost_it.rank() != idx) {
      std::cout << ost_it.rank() << " " << idx << std::endl;
      std::cout.flush();
    }
    assert(ost_it.rank() == idx && "wrong item rank");
    max_height = std::max(max_height, it.height());
  }
  //std::cout << "max height " << max_height << std::endl;
  assert(full.size() == idx && "not all elements were iterated");

  m = full;
  full = dummy;
  assert(dummy.size() == 0 && full.size() == 0 && "data must be released");

  for (std::size_t i = 0; i < 2 * size; ++i) {
    if (i % 2 == 0)
      m = m.insert(data[i], -i);
    else
      m = m.erase(data[i]);
  }

  for (iterator it = m.begin(), end = m.end(); it != end; ++it) {
    assert(std::abs(it.balance_factor()) < 2  && "unbalanced tree");
  }
}

void test_consistency(std::size_t size) {
  std::vector<std::size_t> data;
  data.reserve(2 * size);
  for (std::size_t i = 0; i < 2 * size; ++i) {
    data[i] = i;
  }
  std::random_shuffle(data.begin(), data.end());
  test_consistency(size, data);
}

void test_consistency_left_right() {
  std::vector<int> data;
  data.push_back(8);
  data.push_back(3);
  data.push_back(15);
  data.push_back(5);
  data.push_back(10);
  data.push_back(1);
  data.push_back(20);
  data.push_back(0);
  data.push_back(2);
  data.push_back(4);
  data.push_back(6);
  data.push_back(7);
  data.push_back(-1);
  data.reserve(2 * data.size());
  for (std::size_t i = 0, size = data.size(); i < size; ++i) {
    data.push_back(30 + data[i]);
  }

  typedef wheel::immutable_map<int, int> map_type;
  std::size_t size = data.size();// / 2;
  map_type m;
  map_type halfway;
  assert(m.size() == 0 && "garbage size #1");
  for (std::size_t i = 0; i < size; ++i) {
    m = m.insert(data[i], i);
    assert(m.size() == (i+1) && "size mismatch");
  }

  typedef map_type::const_iterator iterator;
  for (iterator it = m.begin(), end = m.end(); it != end; ++it) {
    assert(std::abs(it.balance_factor()) < 2  && "unbalanced tree");
  }
  m = m.erase(8);
  for (iterator it = m.begin(), end = m.end(); it != end; ++it) {
    assert(std::abs(it.balance_factor()) < 2  && "unbalanced tree");
  }
}

int main(int argc, char* argv[]) {

  try {
    {
      test_consistency_left_right();

      //test_consistency(10000000);
      for (std::size_t i = 0; i < 20000; ++i) {
        test_consistency(7);
      }
      test_consistency(150000);
      for (std::size_t i = 0; i < 2000; ++i) {
        test_consistency(i);
      }

      typedef wheel::immutable_map<int, char> map_type;
      map_type root;
      map_type version1 = root.insert(5, 'a');
      map_type version2 = version1.insert(10, 'b');
      map_type version3 = version2.insert(2, 'c');
      map_type version4 = version3.insert(0, 'd');
      map_type version5 = version4.insert(12, 'e');
      typedef map_type::const_iterator iterator;
      iterator it = version4.begin();
      iterator end = version4.end();
      iterator it10 = version5.find(10);
      iterator it0 = version5.find(0);
      iterator it12 = version5.find(12);
      iterator it5 = version5.find(5);
      map_type version6 = version5.erase(2).erase(5).erase(7);
      std::size_t size4 = version4.size();
      std::size_t size5 = version5.size();
      std::size_t size6 = version6.size();
      std::cout << "size4 " << size4 << " size5 " << size5 << " size6 " << size6 << std::endl;
      print_map(root, "root");
      map_type version7 = version5.insert(5, 'x');
      map_type version8 = version7.insert(7, 'y');
      map_type version9 =
          version8.insert(12, 'z').insert(-1, 'm')
          .insert(15, 'n').erase(5).insert(4, 'k');
      std::size_t size7 = version7.size();
      std::size_t size8 = version8.size();
      std::size_t size9 = version9.size();
      print_map(version4, "version4");
      print_map(version5, "version5");
      print_map(version6, "version6");
      print_map(version7, "version7");
      print_map(version8, "version8");
      print_map(version9, "version9");
      std::cout << "size7 " << size7 << " size8 " << size8 << " size9 " << size9 << std::endl;
      iterator ost_it1 = version8.order_statistic(3);
      std::cout << "version8 3rd order statistic " << ost_it1->first << " -> " << ost_it1->second << std::endl;
      std::cout << "version8 3rd order statistic rank " << ost_it1.rank() << std::endl;
      iterator ost_it2 = version9.order_statistic(3);
      std::cout << "version9 3rd order statistic " << ost_it2->first << " -> " << ost_it2->second << std::endl;
      std::cout << "version9 3rd order statistic rank " << ost_it2.rank() << std::endl;
    }
  } catch (const std::exception& ex) {
    std::cerr << "There was no luck: " << ex.what() << std::endl;
  }

  return 0;
}
