/*
Copyright (c) 2016, Konstantin Chigintsev <kchigintsev@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __WHEEL_IMMUTABLE_MAP__
#define __WHEEL_IMMUTABLE_MAP__

#include <cassert>
#include <stdexcept>
#include <vector>

//#include <boost/shared_ptr.hpp>
//#include <memory>

#include "no_heap_ptr.hpp"

namespace wheel // [re]inventing the wheel
{

template<typename K, typename V>
class immutable_map
{
  struct node;
  //typedef std::shared_ptr<node> node_ptr;
  //typedef boost::shared_ptr<node> node_ptr;
  typedef wheel::no_heap::shared_ptr<node> node_ptr;

  struct node {
    //weak_ptr<node> parent; // impossible in this design
    node_ptr left;
    node_ptr right;
    std::size_t count;
    std::size_t height;
    typedef std::pair<K, V> kv_pair_type;
    kv_pair_type kv_pair;

    node() : count(), height() {}
    node(const kv_pair_type& kv, node_ptr left, node_ptr right)
    : left(left), right(right), count(), height(), kv_pair(kv)
    {
      update_properties();
    }

    void update_properties() {
      std::size_t l_count = 0;
      std::size_t l_height = 0;
      if (left) {
        l_count = left->count;
        l_height = left->height;
      }
      std::size_t r_count = 0;
      std::size_t r_height = 0;
      if (right) {
        r_count = right->count;
        r_height = right->height;
      }
      count = l_count + r_count + 1;
      height = std::max(l_height, r_height) + 1;
    }
  };

  node_ptr root_; // current version

  class iterator
  {
    friend class immutable_map;
    typedef std::vector<node_ptr> path_type;
    path_type path;

  public:
    iterator& operator++() {
      if (path.empty()) {
        throw std::range_error("++ beyond end");
        return *this;
      }

      if (path.back()->right) {
        goto_leftmost_path(*this, path.back()->right);
        return *this;
      }

      // going to the root from some branch
      while (true) {
        node_ptr child = path.back();
        path.pop_back();
        if (path.empty()) {
          break;
        }
        node_ptr parent = path.back();
        if (child == parent->left) {
          // parent is bigger so that it is the next element
          break;
        }
      }
      return *this;
    }

    iterator operator++(int) {
      iterator it = *this;
      ++(*this);
      return it;
    }

    bool operator==(const iterator& other) const {
      if (path.size() != other.path.size()) {
        return false;
      }
      return std::equal(path.begin(), path.end(), other.path.begin());
    }
    bool operator!=(const iterator& other) const {
      return !(*this == other);
    }

    const typename node::kv_pair_type& operator*() const {
      if (path.empty()) {
        throw std::range_error("dereferencing end");
      }
      return path.back()->kv_pair;
    }

    const typename node::kv_pair_type * operator->() const {
      if (path.empty()) {
        throw std::range_error("dereferencing end");
      }
      return &path.back()->kv_pair;
    }

    std::size_t rank() const {
      if (path.empty()) {
        throw std::range_error("element does not exist");
      }
      const K& key = path.back()->kv_pair.first;
      std::size_t offset = 0;
      for (typename path_type::const_iterator it = path.begin(), end = path.end(); it != end; ++it) {
        node_ptr n = *it;
        if (n->kv_pair.first < key) {
          offset += (n->left ? n->left->count + 1 : 1);
        }
      }
      offset += (path.back()->left ? path.back()->left->count : 0);
      return offset;
    }

    int balance_factor() const {
      if (path.empty()) {
        return 0;
      }
      return immutable_map::balance_factor(path.back());
    }

    std::size_t height() const {
      if (path.empty()) {
        return 0;
      }
      return immutable_map::height(path.back());
    }

  private:

    static void goto_leftmost_path(iterator& it, node_ptr n) {
      if (n) {
        it.path.reserve(n->height);
        it.path.push_back(n);
        while (n->left) {
          n = n->left;
          it.path.push_back(n);
        }
      }
    }

    static void goto_rightmost_path(iterator& it, node_ptr n) {
      if (n) {
        it.path.reserve(n->height);
        it.path.push_back(n);
        while (n->right) {
          n = n->right;
          it.path.push_back(n);
        }
      }
    }
  }; // iterator


  // find either a node with the proper key
  // or stop where is nowhere to go any further
  //iterator& find_closest_path(iterator& result, const K& key) const {
  void find_closest_path(iterator& result, const K& key) const {
    //iterator result;
    if (root_) {
      result.path.reserve(root_->height);
    }
    node_ptr n = root_;
    while (n) {
      result.path.push_back(n);
      const K& node_key = n->kv_pair.first;
      if (key < node_key) {
        n = n->left;
      } else if (node_key < key) {
        n = n->right;
      } else {
        break; // n's key is equal to key
      }
    }
    //return result;
  }

  node_ptr rotate_left(node_ptr l) {
    node_ptr r = l->right;
    return node_ptr(new node(r->kv_pair, node_ptr(new node(l->kv_pair, l->left, r->left)), r->right));
  }
  node_ptr rotate_right(node_ptr r) {
    node_ptr l = r->left;
    return node_ptr(new node(l->kv_pair, l->left, node_ptr(new node(r->kv_pair, l->right, r->right))));
  }

  static int balance_factor(node_ptr n) {
    if (!n) {
      return 0;
    }
    int l_height = int(n->left ? n->left->height : 0);
    int r_height = int(n->right ? n->right->height : 0);
    return l_height - r_height;
  }

  static std::size_t height(node_ptr n) {
    if (!n) {
      return 0;
    }
    return n->height;
  }

  node_ptr rebalance_one_node(node_ptr n) {
    if (!n) {
      return n;
    }
    int bf = balance_factor(n);

    if (bf > 1) {
      if (balance_factor(n->left) < 0) {
        // test case test_consistency_left_right
        //assert(false && "shit hit the fan 1");
        n = node_ptr(new node(n->kv_pair, rotate_left(n->left), n->right));
      }
      n = rotate_right(n);
    } else if (bf < -1) {
      if (balance_factor(n->right) > 0) {
        // test_consistency_left_right
        //assert(false && "shit hit the fan 2");
        n = node_ptr(new node(n->kv_pair, n->left, rotate_right(n->right)));
      }
      n = rotate_left(n);
    }
    //assert(std::abs(balance_factor(n)) < 2 && "unbalanced tree");
    return n;
  }

  node_ptr replace_and_rebalance(const iterator& way, node_ptr bottom) {
    node_ptr n = rebalance_one_node(bottom);
    if (way.path.size() > 1) {
      // distance from root to the point from
      std::size_t dist = way.path.size() - 2;
      while (true) {

        node_ptr new_n(new node());
        new_n->kv_pair = way.path[dist]->kv_pair;
        if (way.path[dist]->right == way.path[dist+1]) {
          // we went right
          new_n->right = n;
          new_n->left = way.path[dist]->left;
        } else if (way.path[dist]->left == way.path[dist+1]) {
          // we went left
          new_n->left = n;
          new_n->right = way.path[dist]->right;
        } else {
          assert(false && "impossible by path construction");
        }
        new_n->update_properties();
        n = rebalance_one_node(new_n);
        if (!dist) {
          break;
        }
        --dist;
      }
    }
    return n;
  }

  node_ptr erase_0_1_children_node(iterator& mutable_it, node_ptr to_be_erased) {
    node_ptr n;
    if (to_be_erased->left && to_be_erased->right) {
      // both children
      assert(false && "both children in erase_0_1_children_node");
    } else if (!to_be_erased->left && !to_be_erased->right) {
      // no children
      mutable_it.path.pop_back();
      if (mutable_it.path.empty()) {
        return n;
      } else {
        node_ptr parent = mutable_it.path.back();
        node_ptr new_parent(new node());
        new_parent->kv_pair = parent->kv_pair;
        if (parent->left == to_be_erased) {
          new_parent->right = parent->right;
        } else if (parent->right == to_be_erased) {
          new_parent->left = parent->left;
        } else {
          assert(false && "no link between parent and child");
        }
        new_parent->update_properties();
        n = replace_and_rebalance(mutable_it, new_parent);
      }
    } else {
      // one child
      node_ptr child = (to_be_erased->left ? to_be_erased->left : to_be_erased->right);
      node_ptr new_n(new node(child->kv_pair, child->left, child->right));
      n = replace_and_rebalance(mutable_it, new_n);
    }
    return n;
  }

public:

  immutable_map() {}

  immutable_map(const immutable_map& other) { root_ = other.root_; }

  ~immutable_map() {}

  typedef iterator const_iterator;

  immutable_map insert(const K& key, const V& value) {
    iterator it;
    find_closest_path(it, key);
    node_ptr n(new node());
    n->kv_pair = std::pair<K, V>(key, value);
    if (!it.path.empty()) {
      const K& other_key = it.path.back()->kv_pair.first;
      bool equal = !(key < other_key) && !(other_key < key);
      if (equal) {
        n->right = it.path.back()->right;
        n->left = it.path.back()->left;
      } else {
        n->update_properties();
        node_ptr wrapper(new node());
        wrapper->kv_pair = it.path.back()->kv_pair;
        if (key < other_key) {
          wrapper->left = n;
          wrapper->right = it.path.back()->right;
        } else {
          wrapper->right = n;
          wrapper->left = it.path.back()->left;
        }
        n = wrapper;
      }
    }
    n->update_properties();
    n = replace_and_rebalance(it, n);

    immutable_map m;
    m.root_ = n;
    return m;
  }

  immutable_map erase(const K& key) {
    immutable_map m;
    iterator it;
    find_closest_path(it, key);
    if (it.path.empty()) {
      m.root_ = root_;
      return m;
    }

    const K& other_key = it.path.back()->kv_pair.first;
    bool equal = !(key < other_key) && !(other_key < key);
    if (!equal) {
      m.root_ = root_;
      return m;
    }

    node_ptr n;
    node_ptr to_be_erased = it.path.back();
    if (to_be_erased->left && to_be_erased->right) {
      // both children
      iterator lrm_path;
      iterator::goto_rightmost_path(lrm_path, to_be_erased->left);

      node_ptr lrm = lrm_path.path.back();
      node_ptr new_l = erase_0_1_children_node(lrm_path, lrm);
      node_ptr substitute(new node(lrm->kv_pair, new_l, to_be_erased->right));
      n = replace_and_rebalance(it, substitute);
    } else {
      // no children or one child
      n = erase_0_1_children_node(it, to_be_erased);
    }

    m.root_ = n;
    return m;
  }

  const_iterator find(const K& key) const {
    iterator it;
    find_closest_path(it, key);
    if (it.path.empty()) {
      return end();
    } else {
      const K& node_key = it.path.back()->kv_pair.first;
      bool equal = !(key < node_key) && !(node_key < key);
      return (equal ? it : end());
    }
    return it;
  }

  const_iterator order_statistic(std::size_t idx) const {
    iterator it;
    if (!root_) {
      return it;
    }
    if (idx < 0 || size() <= idx) {
      return it;
    }
    node_ptr n = root_;
    while (n) {
      it.path.push_back(n);
      std::size_t offset = (n->left ? n->left->count : 0);
      if (offset == idx) {
        break;
      } else if (idx < offset) {
        n = n->left;
      } else if (offset < idx) {
        n = n->right;
        idx -= (offset + 1);
      } else {
        assert(false && "violation of trichotomy axiom");
      }
    }
    return it;
  }

  const_iterator begin() const {
    const_iterator result;
    if (root_) {
      iterator::goto_leftmost_path(result, root_);
    }
    return result;
  }

  const_iterator end() const { return const_iterator(); }

  std::size_t size() const {
    if (root_) {
      return root_->count;
    }
    return 0;
  }
}; // class immutable_map

} // namespace wheel

#endif // __WHEEL_IMMUTABLE_MAP__
