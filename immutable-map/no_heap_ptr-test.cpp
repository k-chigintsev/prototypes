/*
Copyright (c) 2016, Konstantin Chigintsev <kchigintsev@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

//#include <memory>
//#include <boost/shared_ptr.hpp>

#include "no_heap_ptr.hpp"

struct test_subject
{
  test_subject(const std::string& comment) : comment_(comment)
  {
    std::cout << "test_subject ctor - " << comment << std::endl;
    std::cout.flush();
  }
  ~test_subject() { std::cout << "test_subject dtor - " << comment_ << std::endl; std::cout.flush(); }
private:
  std::string comment_;
};

int main(int argc, char* argv[]) {

  try {
    using wheel::no_heap::shared_ptr;

    {
      shared_ptr<test_subject> a(new test_subject("test1 - a"));
    }

    {
      shared_ptr<test_subject> a(new test_subject("test2 - a"));
      shared_ptr<test_subject> b(new test_subject("test2 - \tb"));
      {
        shared_ptr<test_subject> d(new test_subject("test2 - \t\td"));
        shared_ptr<test_subject> e(new test_subject("test2 - \t\t\te (between d)"));
        shared_ptr<test_subject> f(new test_subject("test2 - \t\tf"));
        b = f;
      }
    }

    {
      shared_ptr<test_subject> a(new test_subject("test stuff"));
      shared_ptr<test_subject> a_copy(a);
      shared_ptr<test_subject> a_copy2(a);
      shared_ptr<test_subject> a_copy3(a_copy2);
      shared_ptr<test_subject> a_copy4(a_copy2);
      shared_ptr<test_subject> a_copy5(a);
    }

    {
      shared_ptr<test_subject> a(new test_subject("complex test stuff"));
      shared_ptr<test_subject> a_copy(a);
      shared_ptr<test_subject> a_copy2(a);
      shared_ptr<test_subject> a_copy3(a_copy2);
      shared_ptr<test_subject> a_copy4(a_copy2);
      shared_ptr<test_subject> a_copy5(a);

      a = a_copy3;
      a_copy2 = a_copy5;
      a_copy3 = a_copy4;
      a_copy = a;
      a_copy4.reset();
    }

    {
      typedef shared_ptr<test_subject> shared_ptr;
      std::vector<shared_ptr> many_ptrs;
      shared_ptr z(new test_subject("test - z - many instances"));
      {
        shared_ptr copy_constructed(z);
        many_ptrs.push_back(copy_constructed);
      }
      {
        shared_ptr copy_constructed(z);
        many_ptrs.push_back(copy_constructed);
      }
    }

    {
      // opposite to smart ptr
      typedef shared_ptr<test_subject> stupid_ptr;

      stupid_ptr x(new test_subject("test - x - outer scope where a dies"));
      std::vector<stupid_ptr> outer_ptrs;
      {
        stupid_ptr a(new test_subject("test - a - many instances"));
        stupid_ptr b(new test_subject("test - b - same scope"));
        std::vector<stupid_ptr> many_ptrs;
        //const int copies = 30000000;
        const int copies = 300000;
        for (int i = 0; i < copies; ++i)
        {
          stupid_ptr another_b(b);
          if (i % 2)
          {
            stupid_ptr copy_constructed(a);
            many_ptrs.push_back(copy_constructed);
            many_ptrs.push_back(a);
          }
          else
          {
            stupid_ptr assigned;
            assigned = a;
            many_ptrs.push_back(assigned);
          }
        }
        for (int i = 0; i < copies; ++i)
        {
          many_ptrs.push_back(many_ptrs[rand() % many_ptrs.size()]);
          outer_ptrs.push_back(many_ptrs[rand() % many_ptrs.size()]);
        }
        outer_ptrs.swap(many_ptrs);
        stupid_ptr c(new test_subject("test - c - dies before a and b"));
      }
    }
  } catch (const std::exception& ex) {
    std::cerr << "There was no luck: " << ex.what() << std::endl;
  }

  return 0;
}
