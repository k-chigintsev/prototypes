/*
Copyright (c) 2016, Konstantin Chigintsev <kchigintsev@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __WHEEL_NO_HEAP_SHARED_PTR__
#define __WHEEL_NO_HEAP_SHARED_PTR__

namespace wheel // [re]inventing the wheel
{
namespace no_heap
{

template<typename T>
class shared_ptr
{
  typedef T* ptr_type;
  typedef shared_ptr<T> self_type;
public:
  shared_ptr()
  : ptr_(), prev_(), next_()
  {
    init();
  }

  shared_ptr(T * ptr)
  : ptr_(ptr), prev_(), next_()
  {
    init(ptr);
  }

  shared_ptr(const shared_ptr& other)
  {
    shared_ptr& other_unconst = other.unconst();
    init();
    connect(&other_unconst);
  }

  shared_ptr& operator=(const shared_ptr& other)
  {
    if (&other == this)
    {
      return *this;
    }
    shared_ptr& other_unconst = other.unconst();
    drop();
    connect(&other_unconst);
    return *this;
  }

  ~shared_ptr()
  {
    drop();
  }

  void reset(T * ptr = ptr_type())
  {
    drop();
    init(ptr);
  }

  T * get() { return ptr_; }
  const T * get() const { return ptr_; }

  T * operator->() { return ptr_; }
  const T * operator->() const { return ptr_; }

  bool operator==(const shared_ptr& other) const {
    return get() == other.get();
  }

  bool operator!=(const shared_ptr& other) const {
    return !((*this) == other);
  }

  bool operator!() const {
    return get() == ptr_type();
  }

  operator bool() const {
    return !(this->operator!());
  }

private:

  void init(T * ptr = ptr_type())
  {
    prev_ = this;
    next_ = this;
    ptr_ = ptr;
  }

  void connect(self_type * other)
  {
    self_type * other_next = other->next_;

    other_next->prev_ = this;
    next_ = other_next;

    other->next_ = this;
    prev_ = other;

    ptr_ = other->ptr_;
  }

  void drop()
  {
    self_type * left = prev_;
    self_type * right = next_;

    bool last_instance = (left == right && left == this);

    if (last_instance)
    {
      if (ptr_)
      {
        delete ptr_;
        ptr_ = ptr_type();
      }
    }
    else
    {
      left->next_ = right;
      right->prev_ = left;
      init();
    }
  }

  self_type & unconst() const
  {
    return const_cast<self_type &>(*this);
  }

  T * ptr_;
  self_type * prev_;
  self_type * next_;
}; // class shared_ptr

} // namespace no_heap
} // namespace wheel

#endif // __WHEEL_NO_HEAP_SHARED_PTR__
