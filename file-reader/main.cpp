#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <iostream>
#include <sstream>
#include <cstdio>


class file_reader {
  enum { block_size = 9 }; // 4KB would be better for real code
public:
  typedef boost::function<void(const boost::system::error_code& ec, char* buf, std::size_t bytes)> handler_type;

  file_reader(boost::asio::io_service& io_svc, const std::string& name)
  : io_svc_(io_svc)
  , f_()
  {
    FILE *f = std::fopen(name.c_str(), "rb");
    if (!f) {
       throw std::exception();
    }
    f_.reset(f, &std::fclose);
  }

  void async_read(handler_type handler) {
    handler_ = handler;
    async_read_impl();
  }

private:

  void async_read_impl() {
    io_svc_.post( boost::bind(&file_reader::on_read, this) );
  }

  void on_read() {
    std::size_t bytes_read = std::fread(buf_, 1, sizeof(buf_), static_cast<FILE*>(f_.get()));
    bool is_error = std::ferror( static_cast<FILE*>(f_.get()) )  != 0;
    handler_( is_error ? boost::system::error_code(boost::asio::error::invalid_argument) : boost::system::error_code(),
              buf_,
              bytes_read);
    if (bytes_read) {
      async_read_impl();
    }
  }

  boost::asio::io_service& io_svc_;
  handler_type handler_;
  boost::shared_ptr<void> f_;
  char buf_[block_size];
};


class file_writer {
public:
  typedef boost::function<void(char*& buf, std::size_t& bytes)> source_type;

  file_writer(boost::asio::io_service& io_svc, const std::string& name)
  : io_svc_(io_svc)
  , f_()
  {
    //std::cout << __FUNCTION__ << std::endl;
    FILE *f = std::fopen(name.c_str(), "wb");
    if (!f) {
       throw std::exception();
    }
    f_.reset(f, &std::fclose);
  }
  
  void async_write(source_type source) {
    source_ = source;
    async_write_impl();
  }

private:

  void async_write_impl() {
    //std::cout << __FUNCTION__ << std::endl;
    io_svc_.post( boost::bind(&file_writer::on_write, this) );
  }

  void on_write() {
    //std::cout << __FUNCTION__ << std::endl;
    char* buf = 0;
    std::size_t bytes = 0;
    source_(buf, bytes);

    if (buf && bytes) {
      std::fwrite(buf, bytes, 1, static_cast<FILE*>(f_.get()));
      async_write_impl();
    }
  }

  boost::asio::io_service& io_svc_;
  source_type source_;
  boost::shared_ptr<void> f_;
};


void print_hex_buf(char* buf, std::size_t size) {
  char hex[] = "0123456789ABCDEF";
  for (char *c = buf, *end = buf + size; c != end; ++c) {
    std::cout << hex[((*c) >> 4) & 0x0F] << hex[(*c) & 0x0F] << " ";
  }
}


class random_generator
{
  enum { block_size = 16 };
public:
  random_generator(const std::string& id, std::size_t size)
  : id_(id)
  , size_(size)
  {}

  void operator()(char*& buf, std::size_t& bytes) {
    //std::cout << __FUNCTION__ << (void*)buf << " " << &bytes << std::endl;
    bytes = std::min(sizeof(buf_), size_);
    if (!bytes) {
      buf = 0;
      return;
    }

    size_ -= bytes;
    for (int i = 0, end = bytes; i != end; ++i) {
      buf_[i] = std::rand() % (1 << 8);
    }
    std::cout << id_ << " new: ";
    print_hex_buf(buf_, bytes);
    std::cout << std::endl;
    buf = buf_;
  }

private:
  std::string id_;
  std::size_t size_;
  char buf_[block_size];
};


void read_handler(const boost::system::error_code& ec, char* buf, std::size_t bytes, const std::string& id) {
  std::cout << id << " read: ";
  if (buf && bytes) {
    print_hex_buf(buf, bytes);
  }
  std::cout << std::endl;
}



int main(int argc, char* argv[]) {

  try {
    boost::asio::io_service io_svc;

    std::cout << "---- Generating some random files ----" << std::endl;

    const std::size_t files_number = 10;
    typedef boost::shared_ptr<file_writer> writer_ptr;
    std::vector<writer_ptr> writers;
    for (std::size_t f = 0; f < files_number; ++f) {
      std::ostringstream oss;
      oss << "data/file-" << f << ".dat";
      writers.push_back( writer_ptr( new file_writer(io_svc, oss.str() ) ) );
      writers.back()->async_write( random_generator(oss.str(), 35) );
    }

    std::cout << "---- Ready to call io_service::run! Go!  ----" << std::endl;
    io_svc.run();
    writers.clear();
    std::cout << "---- io_service::run finished when all tasks had been completed  ----" << std::endl;

    std::cout << "---- Resetting io_service to prepare it for the next run  ----" << std::endl;
    io_svc.reset();

    typedef boost::shared_ptr<file_reader> reader_ptr;
    std::vector<reader_ptr> readers;
    for (std::size_t f = 0; f < files_number; ++f) {
      std::ostringstream oss;
      oss << "data/file-" << f << ".dat";
      readers.push_back( reader_ptr( new file_reader(io_svc, oss.str() ) ) );
      readers.back()->async_read( boost::bind(&read_handler, _1, _2, _3, oss.str()) );
    }

    io_svc.run();
    std::cout << "---- Done  ----" << std::endl;
  } catch (const boost::system::error_code& ec) {
    std::cerr << "Asio error occurred: " << ec.message() << std::endl;
  } catch (const std::exception& ex) {
    std::cerr << "There was no luck: " << ex.what() << std::endl;
  }

  return 0;
}

